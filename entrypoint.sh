#!/bin/sh
syslogd
/usr/sbin/crond -l 15
mkdir -p /var/spool/postfix/postgrey
chown postgrey /var/spool/postfix/postgrey
chmod 755 /var/spool/postfix/postgrey
postgrey --daemonize --inet=127.0.0.1:10023
spamass-milter -g postfix -p /var/spool/postfix/spamass-milter.sock -f -- --dest=mail_spamassassin -u mail
postfix start
tail -f /var/log/syslog&
tailpid=$!

stop() {
	postfix stop
	killall spamass-milter
	killall postgrey
	killall syslogd
	kill $tailpid
}

trap stop SIGTERM
wait
