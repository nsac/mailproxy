FROM alpine:3.15.4

LABEL maintainer="Richard Brinkman <server@nsac.alpenclub.nl>"

ARG RELAYHOST="[smtp.antispamcloud.com]:587"

EXPOSE 25

RUN apk --no-cache add inetutils-syslogd libmilter libstdc++ logrotate mutt perl-netaddr-ip pflogsumm postfix postgrey spamassassin-client

COPY etc/postfix/ /etc/postfix/
COPY inetutils-syslogd /etc/logrotate.d/inetutils-syslogd

RUN apk --no-cache add --virtual .buildtools autoconf automake curl g++ libmilter-dev make && \
    curl --output /tmp/spamass-milter-master.zip https://codeload.github.com/andybalholm/spamass-milter/zip/master && \
    cd /tmp && \
    unzip spamass-milter-master.zip && \
    cd spamass-milter-master && \
    ./autogen.sh && \
    make && \
    make install && \
    curl --output /etc/postfix/asianblacklist.cidr https://www.okean.com/sinokoreacidr.txt && \
    sed --in-place --expression 's/China/REJECT Mail from China has been disabled due to accessive abuse./;s/Korea/REJECT Mail from Korea has been disabled due to accessive abuse./' /etc/postfix/asianblacklist.cidr && \
    apk --no-cache del .buildtools && \
    cd /tmp && \
    rm -rf spamass-milter-master* && \
    chmod 644 /etc/logrotate.d/inetutils-syslogd && \
    echo 'sa-learn	unix	-	n	-	-	1	pipe user=mail argv=spamc --dest mail_spamassassin --learntype ${nexthop}' >> /etc/postfix/master.cf && \
    postconf -e smtputf8_enable=no && \
    echo "postmaster: root" > /etc/aliases && \
    postalias /etc/aliases && \
    postmap /etc/postfix/address_verification /etc/postfix/helo_access /etc/postfix/transport && \
    chmod 644 /etc/postfix/address_verification /etc/postfix/asianblacklist.cidr /etc/postfix/header_checks /etc/postfix/milter_header_checks /etc/postfix/transport && \
    postconf -e alias_maps=lmdb:/etc/aliases && \
    postconf -e always_add_missing_headers=yes && \
    postconf -e body_checks=regexp:/etc/postfix/body_checks && \
    postconf -e default_destination_concurrency_limit=5 &&\
    postconf -e default_destination_concurrency_negative_feedback=0.2 && \
    postconf -e default_destination_concurrency_positive_feedback=0.1 && \
    postconf -e destination_concurrency_feedback_debug=yes && \
    postconf -e header_checks=regexp:/etc/postfix/header_checks && \
    postconf -e transport_maps=lmdb:/etc/postfix/transport && \
    postconf -e local_header_rewrite_clients=permit_inet_interfaces,permit_mynetworks && \
    postconf -e mail_spool_directory=/var/spool/mail && \
    postconf -e milter_header_checks=regexp:/etc/postfix/milter_header_checks && \
    postconf -e myhostname=mx0.alpenclub.nl && \
    postconf -e mydomain=alpenclub.nl && \
    postconf -e myorigin=alpenclub.nl && \
    postconf -e mynetworks=127.0.0.0/8,172.16.0.0/12,192.168.0.0/16,185.62.57.82 && \
    postconf -e notify_classes=bounce,resource,software && \
    postconf -e recipient_canonical_maps=tcp:mail_postsrsd:10002 && \
    postconf -e recipient_canonical_classes=envelope_recipient,header_recipient && \
    postconf -e relay_domains=alpenclub.nl,esac.nl && \
    postconf -e relayhost=$RELAYHOST && \
    postconf -e "smtpd_client_restrictions=permit_mynetworks,check_client_access cidr:/etc/postfix/asianblacklist.cidr,check_helo_access lmdb:/etc/postfix/helo_access,reject_unknown_client_hostname,reject_rbl_client zen.spamhaus.org=127.0.0.[2..11],check_policy_service inet:127.0.0.1:10023" &&\
    postconf -e sender_canonical_maps=tcp:mail_postsrsd:10001 && \
    postconf -e sender_canonical_classes=envelope_sender && \
    postconf -e "smtpd_helo_restrictions=reject_invalid_helo_hostname,reject_non_fqdn_hostname,check_helo_access lmdb:/etc/postfix/helo_access,reject_unknown_helo_hostname" && \
    postconf -e smtpd_helo_required=yes && \
    postconf -e "smtpd_recipient_restrictions=check_recipient_access lmdb:/etc/postfix/address_verification" && \
    postconf -e smtpd_sender_restrictions=reject_non_fqdn_sender,reject_unknown_sender_domain,reject_unverified_sender && \
    postconf -e smtpd_data_restrictions=reject_unauth_pipelining && \
    postconf -e smtp_tls_security_level=may && \
    postconf -e "milter_connect_macros=i j {daemon_name} v {if_name} _" && \
    postconf -e smtpd_milters=unix:/var/spool/postfix/spamass-milter.sock,inet:mail_dkim:8891 && \
    postconf -e non_smtpd_milters=unix:/var/spool/postfix/spamass-milter.sock,inet:mail_dkim:8891 && \
    postconf -e smtpd_tls_security_level=may && \
    postconf -e smtpd_tls_cert_file=/etc/nginx/certs/mx0.alpenclub.nl/cert.pem && \
    postconf -e smtpd_tls_key_file=/etc/nginx/certs/mx0.alpenclub.nl/key.pem && \
    postconf -e smtpd_tls_loglevel=1 && \
    postconf -e unknown_client_reject_code=550 && \
    postconf -e unverified_recipient_reject_code=550 && \
    newaliases && \
    echo set use_envelope_from=yes > /root/.muttrc

COPY entrypoint.sh /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
