import base64
import sys


def print_rule(content):
    print('/{}/ REJECT Content looks like SPAM'.format(content.replace(b'/', b'\\/').decode()))


bytestring = b' '.join(bytes(arg, 'UTF-8') for arg in sys.argv[1:])

# ASCII
print_rule(bytestring)

# quoted printable
if bytestring.find(b' ') >= 0:
    print_rule(bytestring.replace(b' ', b'=20'))

# Base 64
for _ in range(3):
    chars_to_strip = len(bytestring) % 3
    if chars_to_strip > 0:
        print_rule(base64.encodebytes(bytestring[:-chars_to_strip]).replace(b'\n', b''))
    else:
        print_rule(base64.encodebytes(bytestring).replace(b'\n', b''))
    bytestring = bytestring[1:]
